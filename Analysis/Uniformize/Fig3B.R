
library(GenomicRanges)
library(GenomicFeatures)
library(ChIPseeker)
library(rtracklayer)
library(TxDb.Hsapiens.UCSC.hg19.knownGene)

localPaperRoot <- "C:/Dev/Projects/Papier_Cancer"
source(file.path(localPaperRoot, "Analysis/Shared.R"))

do.fig.3B <- function(filePattern="100.bed$") {
  push.output.stack("Fig3B")
  
  tryCatch({
    overlaps <- load.overlap.annotation(filePattern)
    tf.on.tf <- characterize.tf.on.tf(overlaps$Overlaps, overlaps$ChromatinStates, overlaps$AllFactors)
  }, finally = {
    pop.output.stack() 
  })
}

load.overlap.annotation <- function(filePattern="100.bed$") {
  # Import all regions for all cell lines.
  set.output.dir("Core-circuitry-search", get.cell.lines())
  
  # Load all regions.
  regions <- list()
  chromState <- list()
  cellLines <- c("A549", "MCF7", "HEPG2")
  for(cellLine in cellLines) {
    # Load all regions for all TFs
    regions[[cellLine]] <- import.for.cell.line(cellLine, MSNC=FALSE, removeSMC1A=FALSE, reorder=TRUE, removeHistones=TRUE, filePattern=filePattern)
    regions[[cellLine]] <- combine.Factors(regions[[cellLine]], c("NIPBL", "MED1", "SMC1A"), removeOld=TRUE)
    
    # Load chromatin states when available.
    if(cellLine %in% c("A549", "HEPG2")) {
      chromState[[cellLine]] <- import.chrom.state.for.cell.line(cellLine)
    }
  }
  
  # Rename ERa to ESR1 so that it matches the loaded annotations
  names(regions$MCF7)[names(regions$MCF7)=="ERa"] <- "ESR1"
  
  # Get a list of all transcription factors.
  all.factors <- c()
  for(cellLine in cellLines) {
    all.factors <- c(all.factors, names(regions[[cellLine]]))
  }
  all.factors <- unique(all.factors)
  
  # Annotate all NIPBL-MED1-SMC1A-TF overlaps.
  annotatedOverlaps <- list()
  for(cellLine in cellLines) {
    annotatedOverlaps[[cellLine]] <- list()

    # Loop over all TFs
    for(tf in names(regions[[cellLine]])) {
      #Intersect TFs with NIPBL-MED1-SMC1A regions
      overlap <- subsetByOverlaps(regions[[cellLine]]$"NIPBL-MED1-SMC1A", regions[[cellLine]][[tf]])
      
      if(length(overlap) > 0) {
        # Annotate the overlap
        tfAnnotation <- annotatePeak(overlap,
                                     tssRegion=c(-3000, 3000), 
                                     TxDb=TxDb.Hsapiens.UCSC.hg19.knownGene, 
                                     annoDb="org.Hs.eg.db")
        annotatedOverlaps[[cellLine]][[tf]] <- as.data.frame(tfAnnotation)
        
        push.output.stack(cellLine)
        
        tryCatch({
          # Write out annotation
          fullAnnotFile <- paste(tf, " overlap with NIPBL-MED1-SMC1A, full annotation.txt", sep="")
          write.table(annotatedOverlaps[[cellLine]][[tf]],
                      file=file.path(get.output.stack.dir(), fullAnnotFile),
                      sep="\t", col.names=TRUE, row.names=FALSE, quote=FALSE)
        }, finally = {
          pop.output.stack()
        })
      }
    }
  }
  
  return(list(Overlaps=annotatedOverlaps, ChromatinStates=chromState, AllFactors=all.factors))
}


# Given an annotated overlap and a gene symbol, figures out what is the
# "best" region type matching the symbol (Promoter, 5'UTR, exon, etc.)
# within the annotation.
get.best.region.for.tf <- function(annotatedOverlap, tf) {
  # Get all annotations overlapping the target TF
  for(priority in c("Promoter", "5' UTR", "Exon", "Intron", "3' UTR", "Intergenic")) {
    found <- any(grepl(priority, annotatedOverlap$annotation))
    if(found) {
      return(priority)
    }
  }
  
  return("None")
}

# Given an annotated set of genomic regions and chromatin states, finds out what 
# is the "best" chromatin state overlapping the given regions.
get.best.state.for.tf <- function(annotatedOverlap, chromState, tf) {
  statePriority <- c("1_TssA", "2_TssFlnk", "3_TssFlnkU", "4_TssFlnkD",
                     "9_EnhA1", "10_EnhA2", "7_EnhG1", "8_EnhG2",
                     "11_EnhWk", "14_TssBiv", "15_EnhBiv",
                     "5_Tx", "6_TxWk", 
                     "12_ZNF/Rpts", "13_Het", "16_ReprPC", "17_ReprPCWk", "18_Quies")
  for(priority in statePriority) {
    stateOverlap <- subsetByOverlaps(chromState[[priority]], makeGRangesFromDataFrame(annotatedOverlap))
    if(length(stateOverlap) > 0) {
      return(priority)
    }
  }
  
  return("None")
}



non.empty.matrix.subset <- function(input.matrix) {
  bool.matrix <- input.matrix!="" & input.matrix!="-"
  return(input.matrix[rowSums(bool.matrix) > 0, colSums(bool.matrix) > 0])
}

characterize.tf.on.tf <- function(annotatedOverlaps, chromState, all.factors) {
  results <- list()
  # Figure out which TFs could possibly regulate which TFs alongside MED1-NIPBL-SMC1A.
  for(cellLine in get.cell.lines()) {
    full.regions <- matrix("", nrow=length(all.factors), ncol=length(all.factors), dimnames=list(all.factors, all.factors))
    full.states <- full.regions
    
    # Loop over all TFs for which we have Chip-Seq data in this cell line.
    for(tf.overlapping.mns in names(annotatedOverlaps[[cellLine]])) {
      annotation.overlap <- annotatedOverlaps[[cellLine]][[tf.overlapping.mns]]
      
      # Loop over all TFs which could be regulated by the query TF.
      for(tf.gene in all.factors) {
        
        # Get all annotations overlapping the subject TF
        whichRows <- which(annotation.overlap$SYMBOL == tf.gene)
        if(length(whichRows) > 0) {
          annotation.subset <- annotation.overlap[whichRows,, drop=FALSE]
          
          # Figure out what is the "best" region/state in the overlap.
          full.regions[tf.gene, tf.overlapping.mns] <- get.best.region.for.tf(annotation.subset, tf.gene)
          
          if(cellLine %in% c("A549", "HEPG2")) {
            full.states[tf.gene, tf.overlapping.mns] <- get.best.state.for.tf(annotation.subset, chromState[[cellLine]], tf.gene)
          }
        } else {
          full.regions[tf.gene, tf.overlapping.mns] <- "-"
          full.states[tf.gene, tf.overlapping.mns] <- "-"
        }
      }
    }
    
    push.output.stack(cellLine)
    tryCatch({  
      write.table(non.empty.matrix.subset(full.regions), 
                  file=file.path(get.output.stack.dir(), "Regions.txt"),
                  sep="\t", col.names=NA, row.names=TRUE, quote=FALSE)
      write.table(non.empty.matrix.subset(full.states), 
                  file=file.path(get.output.stack.dir(), "States.txt"),
                  sep="\t", col.names=NA, row.names=TRUE, quote=FALSE)
    }, finally = {
      pop.output.stack()
    })
    results[[cellLine]]$Regions <- full.regions
    results[[cellLine]]$States <- full.states
  }
  
  output.param.list("TF self-regulation")
  return(results)
}